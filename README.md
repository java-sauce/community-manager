# CommunityManager

#### 介绍

疫情时期社区人员流动管理系统

初次进行开发，可能存在诸多问题，感谢大家批评指正

#### 环境介绍

1. 基于java8，springboot2.x
2. 数据库采用Mysql，版本8.x
3. 前端框架采用LayUIMINI
4. sql文件放在resources/sql下

#### 安装教程

1. 在线测试地址http://8.142.4.228:8081/  登录账号：1004 密码 1111
2. 希望大家不要恶意删除数据

#### 使用说明

1. 验证码找回功能不能实现，我将我得阿里云秘钥删除了，你可以填写自己的。
2. 数据库数据可以自行添加，密码采用MD5加密，首次使用需要使用项目中MD5工具类将转化的密码放到数据库中

#### 参与贡献

1. 感谢大家的支持，小白刚刚开始，不足之处欢迎指正。
2. 联系微信：FluencyCode
3. 问题可以在下方评论区留言。

#### 特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5. Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
