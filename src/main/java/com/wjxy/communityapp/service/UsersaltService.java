package com.wjxy.communityapp.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.wjxy.communityapp.dto.UserDto;
import com.wjxy.communityapp.dto.UsersaltDto;
import com.wjxy.communityapp.entity.UsersaltEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wjxy.communityapp.utils.Result;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Fluency
 * @since 2021-03-05
 */
public interface UsersaltService extends IService<UsersaltDto> {


    Result addSalt(int count,int role);

    IPage<UsersaltDto> querySaltByParam(Integer currentPage, Integer limit, String queryParams);

    Result deleteSalt(Integer[] saltId);

}
