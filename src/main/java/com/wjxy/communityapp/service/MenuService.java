package com.wjxy.communityapp.service;

import com.wjxy.communityapp.dto.MenuDTO;
import com.wjxy.communityapp.entity.MenuEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Fluency
 * @since 2021-03-30
 */
public interface MenuService extends IService<MenuEntity> {

    List<MenuDTO> listByUserId(Integer userId);

}
