package com.wjxy.communityapp.service;

import com.wjxy.communityapp.entity.ResAddEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 地址住户关联表 服务类
 * </p>
 *
 * @author Fluency
 * @since 2021-01-23
 */
public interface ResAddService extends IService<ResAddEntity> {

}
