package com.wjxy.communityapp.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wjxy.communityapp.dto.VistorDto;
import com.wjxy.communityapp.entity.VisitorEntity;
import com.wjxy.communityapp.mapper.AddressMapper;
import com.wjxy.communityapp.mapper.VisitorMapper;
import com.wjxy.communityapp.service.VisitorService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wjxy.communityapp.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 来访客人登记表 服务实现类
 * </p>
 *
 * @author Fluency
 * @since 2021-01-11
 */
@Service
public class VisitorServiceImpl extends ServiceImpl<VisitorMapper, VisitorEntity> implements VisitorService {

    @Autowired
    private VisitorMapper visitorMapper;
    @Autowired
    private AddressMapper addressMapper;


    @Override
    public IPage<VistorDto> queryVistorByParam(Integer currentPage, Integer limit, String keyword) {
        Page<VistorDto> page = new Page<>();
        return visitorMapper.queryvisitorByParam(page,keyword);
    }

    @Override
    public Result addVistor(VistorDto vistorDto) {
        if (addressMapper.queryVisIdByparmas(vistorDto.getAddHouse(),vistorDto.getAddUnit()) == null)
        {
            return Result.fail("添加的访客地址不存在！！！");
        }else {
            vistorDto.setAddId(addressMapper.queryVisIdByparmas(vistorDto.getAddHouse(),vistorDto.getAddUnit()));
            visitorMapper.addVistor(vistorDto);
            return Result.ok("增加访客"+vistorDto.getVisName()+"成功");
        }
    }

    @Override
    public Result deleteVistors(Integer[] ids) {
        if (ids.length > 0 ){
            visitorMapper.deleteVistor(ids);
            return Result.ok("删除游客成功！");
        }else {
            return Result.fail("所选用户编号为空，删除失败！！！");
        }


    }

    @Override
    public Result updateVistor(VistorDto vistorDto) {
        if (addressMapper.queryVisIdByparmas(vistorDto.getAddHouse(),vistorDto.getAddUnit()) == null)
        {
            return Result.fail("更新的访客地址不存在！！！");
        }else {
            vistorDto.setAddId(addressMapper.queryVisIdByparmas(vistorDto.getAddHouse(),vistorDto.getAddUnit()));
            visitorMapper.updateVistor(vistorDto);
            return Result.ok("更新游客"+vistorDto.getVisName()+"成功");
        }
    }

    @Override
    public IPage<VistorDto> queryVistorByParam() {
        Page<VistorDto> page = new Page<>();
        return visitorMapper.queryvisitorByParam(page,null);
    }
}
