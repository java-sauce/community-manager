package com.wjxy.communityapp.service.impl;

import com.wjxy.communityapp.entity.ResAddEntity;
import com.wjxy.communityapp.mapper.ResAddMapper;
import com.wjxy.communityapp.service.ResAddService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 地址住户关联表 服务实现类
 * </p>
 *
 * @author Fluency
 * @since 2021-01-23
 */
@Service
public class ResAddServiceImpl extends ServiceImpl<ResAddMapper, ResAddEntity> implements ResAddService {

}
