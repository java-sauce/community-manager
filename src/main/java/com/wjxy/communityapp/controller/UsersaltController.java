package com.wjxy.communityapp.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.wjxy.communityapp.dto.UserDto;
import com.wjxy.communityapp.dto.UsersaltDto;
import com.wjxy.communityapp.entity.UsersaltEntity;
import com.wjxy.communityapp.service.UserService;
import com.wjxy.communityapp.service.UsersaltService;
import com.wjxy.communityapp.utils.JsonObject;
import com.wjxy.communityapp.utils.Result;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Fluency
 * @since 2021-03-05
 *
 * 注册码相关
 */
@RestController
@RequestMapping("/Usalt")
@RequiresRoles(value = "admin" , logical = Logical.OR)
public class UsersaltController {
    //添加注册码
    @Autowired
    private UsersaltService usersaltService;

    @GetMapping("/querySaltByParam")
    public JsonObject queryUserByParam(@RequestParam("page")Integer currentPage, @RequestParam("limit")Integer limit)
    {
             JsonObject<UsersaltDto> object = new JsonObject<>();
            IPage<UsersaltDto> dtoIPage = usersaltService.querySaltByParam(currentPage,limit,null);
            object.setCode(0);
            object.setMsg("ok");
            object.setCount(dtoIPage.getTotal());
            object.setData(dtoIPage);
            return object;

    }

    @PostMapping("/addSalt")
    public Result addSalt(Integer count,Integer role)
    {
        return usersaltService.addSalt(count,role);
    }

    @PostMapping("/deleteSalt")
    public Result deleteSalt(Integer[] ids)
    {
        System.out.println(ids);
        return usersaltService.deleteSalt(ids);
    }

    @PostMapping("/updateSalt")
    public Result updateSalt(UsersaltDto usersaltDto)
    {
        return Result.ok();
    }








}

