package com.wjxy.communityapp.mapper;

import com.wjxy.communityapp.dto.RoleAuthDto;
import com.wjxy.communityapp.entity.RoleAuthEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Fluency
 * @since 2021-01-08
 */
public interface RoleAuthMapper extends BaseMapper<RoleAuthEntity> {

    List<RoleAuthDto> getAuthByRole(int roleId);
}
