package com.wjxy.communityapp.mapper;

import com.wjxy.communityapp.entity.AuthorityEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Fluency
 * @since 2021-01-08
 */
public interface AuthorityMapper extends BaseMapper<AuthorityEntity> {

}
