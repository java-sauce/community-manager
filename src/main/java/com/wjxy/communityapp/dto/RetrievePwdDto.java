package com.wjxy.communityapp.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author Fluency
 * @creat 2021-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class RetrievePwdDto implements Serializable {
    private String account;
    private String userPhone;
    private String newPwd;
    private String verCode;
}
