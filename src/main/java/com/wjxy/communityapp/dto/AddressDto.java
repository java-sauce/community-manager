package com.wjxy.communityapp.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Fluency
 * @creat 2021-01
 */
@Data
public class AddressDto implements Serializable {

    private Integer addId;

    @ApiModelProperty(value = "地址代号")
    private String addSort;

    @ApiModelProperty(value = "小区名称")
    private String addName;

    private Integer addHouse;

    private Integer addUnit;

    @ApiModelProperty(value = "地址中住户数量")
    private Integer addCount;

    //住户相关信息

    private Integer resId;

    private String resName;

    private String resPhone;


}
