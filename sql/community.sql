/*
 Navicat Premium Data Transfer

 Source Server         : Fluency
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : localhost:3306
 Source Schema         : community

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 05/04/2021 13:35:42
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_address
-- ----------------------------
DROP TABLE IF EXISTS `tb_address`;
CREATE TABLE `tb_address`  (
  `add_id` int(11) NOT NULL AUTO_INCREMENT,
  `add_sort` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '地址代号',
  `add_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '小区名称',
  `add_house` int(11) NOT NULL,
  `add_unit` int(11) NOT NULL,
  `add_count` int(11) NULL DEFAULT 0 COMMENT '地址中住户数量，默认0',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `deleted` int(2) NOT NULL DEFAULT 0,
  PRIMARY KEY (`add_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 66 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_address
-- ----------------------------
INSERT INTO `tb_address` VALUES (1, '370411001', '锦绣花园', 1, 1001, 3, '2021-01-05 13:34:48', '2021-01-25 16:40:26', 0);
INSERT INTO `tb_address` VALUES (2, '370411002', '锦绣花园', 1, 1002, 1, '2021-01-05 13:34:53', '2021-01-25 15:30:11', 0);
INSERT INTO `tb_address` VALUES (3, '370411003', '锦绣花园', 2, 1001, 1, '2021-01-05 13:35:06', '2021-01-25 15:33:15', 0);
INSERT INTO `tb_address` VALUES (4, '370411004', '锦绣花园', 2, 2001, 1, '2021-01-05 13:35:10', '2021-01-25 15:30:01', 0);
INSERT INTO `tb_address` VALUES (5, '370411005', '锦绣花园', 1, 2002, 1, '2021-01-05 13:35:16', '2021-01-25 15:30:01', 0);
INSERT INTO `tb_address` VALUES (8, '370411008', '锦绣花园', 1, 3002, 1, '2021-01-13 15:54:17', '2021-01-25 15:30:01', 0);
INSERT INTO `tb_address` VALUES (9, '370411009', '锦绣花园', 1, 3003, 1, '2021-01-13 15:54:21', '2021-01-25 15:30:01', 0);
INSERT INTO `tb_address` VALUES (10, '370411010', '锦绣花园', 1, 4001, 1, '2021-01-13 15:54:25', '2021-01-25 15:30:01', 0);
INSERT INTO `tb_address` VALUES (11, '370411011', '锦绣花园', 1, 4002, 1, '2021-01-13 15:54:30', '2021-01-25 15:30:01', 0);
INSERT INTO `tb_address` VALUES (12, '370411012', '锦绣花园', 1, 4003, 1, '2021-01-13 15:54:35', '2021-01-25 15:30:01', 0);
INSERT INTO `tb_address` VALUES (13, '370411013', '锦绣花园', 1, 5001, 1, '2021-01-13 15:54:40', '2021-01-25 15:30:01', 0);
INSERT INTO `tb_address` VALUES (14, '370411014', '锦绣花园', 1, 5002, 1, '2021-01-13 15:54:45', '2021-01-25 15:30:01', 0);
INSERT INTO `tb_address` VALUES (15, '370411015', '锦绣花园', 1, 5003, 1, '2021-01-13 15:54:50', '2021-01-25 15:30:01', 0);
INSERT INTO `tb_address` VALUES (16, '370411016', '锦绣花园', 1, 6001, 1, '2021-01-13 15:54:53', '2021-01-25 15:30:01', 0);
INSERT INTO `tb_address` VALUES (17, '370411017', '锦绣花园', 1, 6002, 1, '2021-01-13 15:54:59', '2021-01-25 15:30:01', 0);
INSERT INTO `tb_address` VALUES (18, '370411018', '锦绣花园', 1, 6003, 1, '2021-01-13 15:55:07', '2021-01-25 15:30:01', 0);
INSERT INTO `tb_address` VALUES (19, '370411019', '锦绣花园', 1, 7001, 1, '2021-01-13 15:55:10', '2021-01-25 15:30:01', 0);
INSERT INTO `tb_address` VALUES (20, '370411020', '锦绣花园', 1, 7002, 1, '2021-01-13 15:55:13', '2021-01-25 15:30:01', 0);
INSERT INTO `tb_address` VALUES (21, '370411021', '锦绣花园', 1, 7003, 1, '2021-01-13 15:55:30', '2021-01-25 15:30:01', 0);
INSERT INTO `tb_address` VALUES (22, '370411022', '锦绣花园', 1, 8001, 1, '2021-01-13 15:55:37', '2021-01-25 15:30:01', 0);
INSERT INTO `tb_address` VALUES (23, '370411023', '锦绣花园', 1, 8002, 1, '2021-01-13 15:55:48', '2021-01-25 15:30:01', 0);
INSERT INTO `tb_address` VALUES (24, '370411024', '锦绣花园', 1, 8003, 1, '2021-01-13 15:55:51', '2021-01-25 15:30:01', 0);
INSERT INTO `tb_address` VALUES (25, '370411025', '锦绣花园', 1, 8004, 0, '2021-01-13 15:55:55', '2021-01-25 15:30:01', 0);
INSERT INTO `tb_address` VALUES (26, '370411026', '锦绣花园', 1, 9001, 0, '2021-01-13 15:55:59', '2021-01-25 15:30:01', 0);
INSERT INTO `tb_address` VALUES (27, '370411001', '锦绣花园', 1, 9002, 0, '2021-01-16 16:32:55', '2021-01-25 15:30:01', 0);
INSERT INTO `tb_address` VALUES (28, '370411001', '锦绣花园', 1, 9002, 0, '2021-01-16 16:33:04', '2021-01-25 15:30:01', 0);
INSERT INTO `tb_address` VALUES (29, '370411001', '锦绣花园', 1, 9002, 0, '2021-01-16 16:37:18', '2021-01-25 15:30:01', 0);
INSERT INTO `tb_address` VALUES (30, '370411001', '锦绣花园', 1, 9002, 0, '2021-01-16 17:01:49', '2021-01-25 15:30:01', 0);
INSERT INTO `tb_address` VALUES (31, '370411001', '锦绣花园', 1, 9002, 0, '2021-01-16 17:02:27', '2021-01-25 15:30:01', 0);
INSERT INTO `tb_address` VALUES (32, '370411001', '锦绣花园', 3, 1001, 0, '2021-01-16 17:03:07', '2021-01-25 15:30:01', 0);
INSERT INTO `tb_address` VALUES (33, '370411001', '锦绣花园', 1, 6004, 0, '2021-01-16 17:07:36', '2021-01-25 15:30:01', 0);
INSERT INTO `tb_address` VALUES (61, '370411001', '锦绣花园', 5, 5001, 0, '2021-01-23 22:29:34', '2021-01-25 15:30:01', 0);
INSERT INTO `tb_address` VALUES (62, NULL, '锦绣花园', 3, 1003, 0, '2021-01-25 13:20:50', '2021-01-25 15:30:01', 0);
INSERT INTO `tb_address` VALUES (63, NULL, '锦绣花园', 3, 3004, 0, '2021-01-25 13:49:12', '2021-01-25 15:30:01', 0);
INSERT INTO `tb_address` VALUES (64, NULL, '锦绣花园', 10, 1003, 0, '2021-01-25 15:48:00', '2021-01-25 16:05:29', 0);
INSERT INTO `tb_address` VALUES (65, '370411001', '锦绣花园', 1, 1004, 1, '2021-01-25 16:16:55', '2021-01-25 16:16:55', 0);

-- ----------------------------
-- Table structure for tb_authority
-- ----------------------------
DROP TABLE IF EXISTS `tb_authority`;
CREATE TABLE `tb_authority`  (
  `auth_id` int(11) NOT NULL AUTO_INCREMENT,
  `auth_name` varchar(99) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '系统管理员等级1 普通管理员等级2',
  `auth_describe` varchar(99) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`auth_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_authority
-- ----------------------------
INSERT INTO `tb_authority` VALUES (1, '增加管理员信息', '超级管理员具有权限', '2021-01-05 13:32:57', '2021-01-08 12:59:58');
INSERT INTO `tb_authority` VALUES (2, '修改管理员信息', '超级管理员权限', '2021-01-05 13:33:04', '2021-01-08 12:59:46');
INSERT INTO `tb_authority` VALUES (3, '增加住户信息', '通用功能', '2021-01-08 12:58:58', '2021-01-08 12:59:30');
INSERT INTO `tb_authority` VALUES (4, '修改住户信息', '通用功能', '2021-01-08 12:59:10', '2021-01-08 12:59:34');
INSERT INTO `tb_authority` VALUES (5, '添加访问记录', '通用功能', '2021-01-08 12:59:22', '2021-01-08 13:01:10');
INSERT INTO `tb_authority` VALUES (6, '修改访问记录', '通用功能', '2021-01-08 13:00:19', '2021-01-08 13:01:11');
INSERT INTO `tb_authority` VALUES (7, '管理员修改个人信息', '通用功能', '2021-01-08 13:01:07', '2021-01-08 13:01:07');

-- ----------------------------
-- Table structure for tb_menu
-- ----------------------------
DROP TABLE IF EXISTS `tb_menu`;
CREATE TABLE `tb_menu`  (
  `menu_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称',
  `parent_id` int(10) NOT NULL COMMENT '父菜单ID',
  `href` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单路径',
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `target` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `deleted` int(2) NOT NULL DEFAULT 0 COMMENT '0表示未删除 1删除',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_menu
-- ----------------------------
INSERT INTO `tb_menu` VALUES (1, '常规管理', -1, NULL, 'fa fa-address-book', '_self', 0);
INSERT INTO `tb_menu` VALUES (2, '系统管理', -1, NULL, 'fa fa-lemon-o', '_self', 0);
INSERT INTO `tb_menu` VALUES (3, '其他管理', -1, NULL, 'fa fa-meetup', '_self', 0);
INSERT INTO `tb_menu` VALUES (4, '住户模块', 1, NULL, 'fa fa-home', '_self', 0);
INSERT INTO `tb_menu` VALUES (5, '出入模块', 1, NULL, 'fa fa-window-maximize', '_self', 0);
INSERT INTO `tb_menu` VALUES (6, '访客模块', 1, NULL, 'fa fa-gears', '_self', 0);
INSERT INTO `tb_menu` VALUES (7, '其他模块', 1, NULL, 'fa fa-file-text', '_self', 0);
INSERT INTO `tb_menu` VALUES (8, '住户管理', 4, '/User/resident', 'fa fa-tachometer', '_self', 0);
INSERT INTO `tb_menu` VALUES (9, '高危住户', 4, '/User/perilPage', 'fa fa-tachometer', '_self', 0);
INSERT INTO `tb_menu` VALUES (10, '住户统计', 4, '/User/resCount', 'fa fa-tachometer', '_self', 0);
INSERT INTO `tb_menu` VALUES (11, '住户出入登记', 5, '/User/resRecord', 'fa fa-tachometer', '_self', 0);
INSERT INTO `tb_menu` VALUES (12, '访客出入登记', 5, '/User/visRecord', 'fa fa-tachometer', '_self', 0);
INSERT INTO `tb_menu` VALUES (13, '出入信息管理', 5, '/User/records', 'fa fa-tachometer', '_self', 0);
INSERT INTO `tb_menu` VALUES (14, '访客管理', 6, '/User/vistors', 'fa fa-tachometer', NULL, 0);
INSERT INTO `tb_menu` VALUES (15, '地址表', 7, '/User/address', 'fa fa-tachometer', NULL, 0);
INSERT INTO `tb_menu` VALUES (16, '用户管理', 2, '/User/users', 'fa fa-snowflake-o', NULL, 0);
INSERT INTO `tb_menu` VALUES (17, '注册码', 2, '/User/userSalt', 'fa fa-hourglass-end', NULL, 0);
INSERT INTO `tb_menu` VALUES (18, '关于我', 3, '/User/sys/aboutMe', NULL, NULL, 0);

-- ----------------------------
-- Table structure for tb_menu_role
-- ----------------------------
DROP TABLE IF EXISTS `tb_menu_role`;
CREATE TABLE `tb_menu_role`  (
  `mu_id` int(10) NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) NOT NULL COMMENT '菜单表ID',
  `role_id` int(10) NOT NULL COMMENT '角色表ID',
  PRIMARY KEY (`mu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_menu_role
-- ----------------------------
INSERT INTO `tb_menu_role` VALUES (1, 1, 1);
INSERT INTO `tb_menu_role` VALUES (2, 2, 1);
INSERT INTO `tb_menu_role` VALUES (3, 3, 1);
INSERT INTO `tb_menu_role` VALUES (4, 4, 1);
INSERT INTO `tb_menu_role` VALUES (5, 5, 1);
INSERT INTO `tb_menu_role` VALUES (6, 6, 1);
INSERT INTO `tb_menu_role` VALUES (7, 7, 1);
INSERT INTO `tb_menu_role` VALUES (8, 8, 1);
INSERT INTO `tb_menu_role` VALUES (9, 9, 1);
INSERT INTO `tb_menu_role` VALUES (10, 10, 1);
INSERT INTO `tb_menu_role` VALUES (11, 11, 1);
INSERT INTO `tb_menu_role` VALUES (12, 12, 1);
INSERT INTO `tb_menu_role` VALUES (13, 13, 1);
INSERT INTO `tb_menu_role` VALUES (14, 1, 2);
INSERT INTO `tb_menu_role` VALUES (15, 4, 2);
INSERT INTO `tb_menu_role` VALUES (16, 5, 2);
INSERT INTO `tb_menu_role` VALUES (17, 6, 2);
INSERT INTO `tb_menu_role` VALUES (18, 7, 2);
INSERT INTO `tb_menu_role` VALUES (19, 8, 2);
INSERT INTO `tb_menu_role` VALUES (20, 9, 2);
INSERT INTO `tb_menu_role` VALUES (21, 10, 2);
INSERT INTO `tb_menu_role` VALUES (22, 14, 1);
INSERT INTO `tb_menu_role` VALUES (23, 15, 1);
INSERT INTO `tb_menu_role` VALUES (24, 16, 1);
INSERT INTO `tb_menu_role` VALUES (25, 17, 1);
INSERT INTO `tb_menu_role` VALUES (26, 18, 1);
INSERT INTO `tb_menu_role` VALUES (27, 10, 2);
INSERT INTO `tb_menu_role` VALUES (28, 11, 2);
INSERT INTO `tb_menu_role` VALUES (29, 12, 2);
INSERT INTO `tb_menu_role` VALUES (30, 13, 2);
INSERT INTO `tb_menu_role` VALUES (31, 14, 2);
INSERT INTO `tb_menu_role` VALUES (32, 15, 2);

-- ----------------------------
-- Table structure for tb_record
-- ----------------------------
DROP TABLE IF EXISTS `tb_record`;
CREATE TABLE `tb_record`  (
  `rec_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `rec_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `rec_type` int(2) NOT NULL COMMENT '进入是1 出去是0',
  `rec_personId` int(11) NULL DEFAULT NULL COMMENT '进出人员的信息ID',
  `rec_perType` int(2) NULL DEFAULT NULL COMMENT '住户是0 访客是1',
  `rec_orderadd` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `rec_tempera` float(10, 2) NOT NULL,
  `rec_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `deleted` int(2) NOT NULL DEFAULT 0,
  PRIMARY KEY (`rec_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_record
-- ----------------------------
INSERT INTO `tb_record` VALUES ('1001', '2021-01-05 13:35:59', 1, 1, 0, '步行街', 36.50, '上班回家', '2021-01-05 13:35:59', '2021-01-22 21:19:29', 0);
INSERT INTO `tb_record` VALUES ('1002', '2021-01-21 18:21:03', 1, 3, 0, '至善小区', 36.50, '访亲', '2021-01-21 18:21:29', '2021-01-22 15:59:07', 0);
INSERT INTO `tb_record` VALUES ('1003', '2021-01-21 21:05:29', 1, 3, 1, '美食街', 36.70, '送外卖', '2021-01-21 21:05:51', '2021-01-22 21:07:39', 0);
INSERT INTO `tb_record` VALUES ('1004', '2021-01-23 17:06:32', 0, 6, 0, '滕州', 36.60, '上班', '2021-01-23 17:06:50', '2021-01-23 17:47:51', 0);
INSERT INTO `tb_record` VALUES ('1005', '2021-01-23 17:45:02', 1, 5, 1, '徐州', 36.50, '探亲', '2021-01-23 17:45:21', '2021-01-23 17:51:35', 0);
INSERT INTO `tb_record` VALUES ('1006', '2021-01-24 11:58:34', 1, 6, 1, '济宁', 36.50, '探亲', '2021-01-24 11:58:58', '2021-01-24 11:58:58', 0);
INSERT INTO `tb_record` VALUES ('1007', '2021-01-23 17:51:43', 1, 3, 0, '创业街', 36.50, '回家', '2021-01-23 17:51:56', '2021-01-23 17:51:56', 0);
INSERT INTO `tb_record` VALUES ('1008', '2021-01-23 17:51:58', 0, 3, 0, '创业街', 36.50, '上班', '2021-01-23 17:52:08', '2021-01-23 17:52:08', 0);
INSERT INTO `tb_record` VALUES ('1009', '2021-01-28 13:58:15', 1, 1, 0, '和谐广场', 37.50, '购物', '2021-01-28 13:58:28', '2021-01-28 13:58:28', 0);
INSERT INTO `tb_record` VALUES ('1010', '2021-01-28 14:13:43', 1, 5, 0, '济南', 36.50, '有事', '2021-01-28 14:13:55', '2021-01-28 14:13:55', 0);
INSERT INTO `tb_record` VALUES ('1011', '2021-01-29 17:43:43', 0, 2, 0, '济宁', 36.60, '有事', '2021-01-29 17:43:43', '2021-01-29 17:43:43', 0);

-- ----------------------------
-- Table structure for tb_res_add
-- ----------------------------
DROP TABLE IF EXISTS `tb_res_add`;
CREATE TABLE `tb_res_add`  (
  `skip_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '地址住户关联表',
  `add_id` int(11) NOT NULL COMMENT '地址ID',
  `res_id` int(11) NOT NULL COMMENT '住户ID',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `deleted` int(2) NULL DEFAULT 0,
  PRIMARY KEY (`skip_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '地址住户关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_res_add
-- ----------------------------
INSERT INTO `tb_res_add` VALUES (1, 1, 1, '2021-01-23 21:17:39', '2021-01-23 21:17:39', 0);
INSERT INTO `tb_res_add` VALUES (2, 2, 2, '2021-01-23 21:19:06', '2021-01-23 21:19:06', 0);
INSERT INTO `tb_res_add` VALUES (3, 3, 3, '2021-01-23 21:19:10', '2021-01-23 21:19:10', 0);
INSERT INTO `tb_res_add` VALUES (4, 5, 5, '2021-01-23 21:19:13', '2021-01-23 21:19:13', 0);
INSERT INTO `tb_res_add` VALUES (5, 8, 6, '2021-01-23 21:19:15', '2021-01-23 21:19:15', 0);
INSERT INTO `tb_res_add` VALUES (6, 9, 16, '2021-01-23 21:19:26', '2021-01-23 21:19:26', 0);
INSERT INTO `tb_res_add` VALUES (7, 10, 17, '2021-01-23 21:19:44', '2021-01-23 21:19:44', 0);
INSERT INTO `tb_res_add` VALUES (8, 11, 18, '2021-01-23 21:20:02', '2021-01-23 21:20:02', 0);
INSERT INTO `tb_res_add` VALUES (9, 12, 19, '2021-01-23 21:20:06', '2021-01-23 21:20:06', 0);
INSERT INTO `tb_res_add` VALUES (10, 13, 20, '2021-01-23 21:20:09', '2021-01-23 21:20:09', 0);
INSERT INTO `tb_res_add` VALUES (11, 14, 21, '2021-01-23 21:20:12', '2021-01-23 21:20:12', 0);
INSERT INTO `tb_res_add` VALUES (12, 15, 22, '2021-01-23 21:20:16', '2021-01-23 21:20:16', 0);
INSERT INTO `tb_res_add` VALUES (13, 16, 23, '2021-01-23 21:20:20', '2021-01-23 21:20:20', 0);
INSERT INTO `tb_res_add` VALUES (14, 17, 24, '2021-01-23 21:20:26', '2021-01-23 21:20:26', 0);
INSERT INTO `tb_res_add` VALUES (15, 18, 25, '2021-01-23 21:22:38', '2021-01-23 21:22:38', 0);
INSERT INTO `tb_res_add` VALUES (16, 19, 26, '2021-01-23 21:22:41', '2021-01-23 21:22:41', 0);
INSERT INTO `tb_res_add` VALUES (17, 20, 27, '2021-01-23 21:22:44', '2021-01-23 21:22:44', 0);
INSERT INTO `tb_res_add` VALUES (18, 21, 28, '2021-01-23 21:22:47', '2021-01-23 21:22:47', 0);
INSERT INTO `tb_res_add` VALUES (19, 22, 29, '2021-01-23 21:22:50', '2021-01-23 21:22:50', 0);
INSERT INTO `tb_res_add` VALUES (20, 23, 30, '2021-01-23 21:22:53', '2021-01-23 21:22:53', 0);
INSERT INTO `tb_res_add` VALUES (21, 24, 31, '2021-01-23 21:22:57', '2021-01-23 21:22:57', 0);
INSERT INTO `tb_res_add` VALUES (22, 25, 32, '2021-01-23 21:23:00', '2021-01-23 21:23:00', 0);
INSERT INTO `tb_res_add` VALUES (23, 26, 33, '2021-01-23 21:23:02', '2021-01-23 21:23:02', 0);
INSERT INTO `tb_res_add` VALUES (24, 4, 34, '2021-01-23 22:29:34', '2021-01-23 22:29:34', 0);
INSERT INTO `tb_res_add` VALUES (25, 65, 35, '2021-01-25 16:16:55', '2021-01-25 16:16:55', 0);
INSERT INTO `tb_res_add` VALUES (26, 1, 36, '2021-01-25 16:37:47', '2021-01-25 16:37:47', 0);
INSERT INTO `tb_res_add` VALUES (27, 1, 37, '2021-01-25 16:40:26', '2021-01-25 16:40:26', 0);

-- ----------------------------
-- Table structure for tb_resident
-- ----------------------------
DROP TABLE IF EXISTS `tb_resident`;
CREATE TABLE `tb_resident`  (
  `res_id` int(11) NOT NULL AUTO_INCREMENT,
  `res_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `res_sex` int(11) NOT NULL DEFAULT 0 COMMENT '0为男性，1为女性',
  `res_phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `res_intotime` date NOT NULL,
  `res_status` int(2) NULL DEFAULT NULL COMMENT '0表示正常，1低风险，2高风险',
  `res_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `res_photo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `deleted` int(2) NOT NULL DEFAULT 0,
  PRIMARY KEY (`res_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1002 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_resident
-- ----------------------------
INSERT INTO `tb_resident` VALUES (1, '王五', 1, '15847856988', '2021-01-15', 0, '无', NULL, '2021-01-05 13:36:25', '2021-01-28 14:29:21', 0);
INSERT INTO `tb_resident` VALUES (2, '李四', 0, '16548956687', '2021-01-15', 0, '无', NULL, '2021-01-12 21:18:52', '2021-01-25 15:33:23', 0);
INSERT INTO `tb_resident` VALUES (3, '马六', 0, '15464884848', '2021-01-15', 1, '无', NULL, '2021-01-12 21:19:16', '2021-01-25 15:33:21', 0);
INSERT INTO `tb_resident` VALUES (5, '李娜', 1, '15488884456', '2021-01-15', 0, '无', NULL, '2021-01-14 19:01:40', '2021-01-23 16:42:30', 0);
INSERT INTO `tb_resident` VALUES (6, '张琳', 1, '15484684618', '2021-01-15', 2, '无', NULL, '2021-01-14 19:02:02', '2021-01-23 16:42:32', 0);
INSERT INTO `tb_resident` VALUES (16, '王刚', 1, '16789547845', '2021-01-01', 0, '无', NULL, '2021-01-16 13:02:34', '2021-01-23 16:58:46', 0);
INSERT INTO `tb_resident` VALUES (17, '王刚', 1, '16789547845', '2021-01-23', 0, '无', NULL, '2021-01-16 13:02:34', '2021-01-23 17:04:02', 0);
INSERT INTO `tb_resident` VALUES (18, '李恒', 1, '16789547845', '2021-01-23', 1, '无', NULL, '2021-01-16 13:02:35', '2021-01-23 22:03:59', 0);
INSERT INTO `tb_resident` VALUES (19, '王刚', 0, '16789547845', '1997-01-01', 0, '无', NULL, '2021-01-16 13:02:36', '2021-01-25 15:30:20', 0);
INSERT INTO `tb_resident` VALUES (20, '王刚', 0, '16789547845', '1997-01-01', 0, '无', NULL, '2021-01-16 13:02:37', '2021-01-25 15:30:21', 0);
INSERT INTO `tb_resident` VALUES (21, '王刚', 0, '16789547845', '1997-01-01', 2, '无', NULL, '2021-01-16 13:02:37', '2021-01-23 16:42:46', 0);
INSERT INTO `tb_resident` VALUES (22, '王刚', 0, '16789547845', '1997-01-01', 0, '无', NULL, '2021-01-16 16:32:55', '2021-01-23 16:42:47', 0);
INSERT INTO `tb_resident` VALUES (23, '王刚', 0, '16789547845', '1997-01-01', 1, '无', NULL, '2021-01-16 16:33:04', '2021-01-23 16:42:48', 0);
INSERT INTO `tb_resident` VALUES (24, '王刚', 0, '16789547845', '1997-01-01', 0, '无', NULL, '2021-01-16 17:01:49', '2021-01-23 21:20:51', 0);
INSERT INTO `tb_resident` VALUES (25, '王刚', 0, '16789547845', '1997-01-01', 0, '无', NULL, '2021-01-16 17:02:27', '2021-01-23 21:20:54', 0);
INSERT INTO `tb_resident` VALUES (26, '王小明', 0, '16478549857', '2021-11-01', 0, '无', NULL, '2021-01-16 17:03:08', '2021-01-23 21:20:56', 0);
INSERT INTO `tb_resident` VALUES (27, '孙菲菲', 0, '16552549857', '2021-11-01', 0, '无', NULL, '2021-01-16 17:07:36', '2021-01-23 21:20:59', 0);
INSERT INTO `tb_resident` VALUES (28, '李乐乐', 1, '16894778452', '2021-01-15', 0, '无', NULL, '2021-01-16 17:14:20', '2021-01-23 21:21:02', 0);
INSERT INTO `tb_resident` VALUES (29, '李乐因', 1, '168947784', '2021-01-15', 1, '无', NULL, '2021-01-16 17:14:31', '2021-01-23 21:21:04', 0);
INSERT INTO `tb_resident` VALUES (30, '李乐乐', 1, '15896478512', '2021-01-14', 0, '无', NULL, '2021-01-16 17:15:40', '2021-01-23 21:21:07', 0);
INSERT INTO `tb_resident` VALUES (31, '马冬梅', 1, '15896874587', '2021-01-15', 0, '无', NULL, '2021-01-16 17:18:23', '2021-01-23 21:21:10', 0);
INSERT INTO `tb_resident` VALUES (32, '李永乐', 0, '14789658745', '2021-01-20', 0, '无', NULL, '2021-01-20 23:21:45', '2021-01-23 21:21:12', 0);
INSERT INTO `tb_resident` VALUES (33, '张似水', 0, '15896478541', '2021-01-21', 0, '我', NULL, '2021-01-21 16:14:28', '2021-01-23 21:21:23', 0);
INSERT INTO `tb_resident` VALUES (34, '李泽惠', 1, '13569874569', '2021-01-23', NULL, '无', NULL, '2021-01-23 22:29:34', '2021-01-23 22:29:34', 0);
INSERT INTO `tb_resident` VALUES (35, '张丽', 1, '13698563251', '2021-01-25', NULL, '无', NULL, '2021-01-25 16:16:55', '2021-01-25 16:16:55', 0);
INSERT INTO `tb_resident` VALUES (36, '马丁', 0, '13658965234', '2021-01-25', 1, '无', NULL, '2021-01-25 16:37:47', '2021-01-28 13:56:31', 0);
INSERT INTO `tb_resident` VALUES (37, '赵武', 0, '13569856539', '2021-01-25', 1, '无', NULL, '2021-01-25 16:40:26', '2021-01-28 13:56:31', 0);

-- ----------------------------
-- Table structure for tb_role
-- ----------------------------
DROP TABLE IF EXISTS `tb_role`;
CREATE TABLE `tb_role`  (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_role
-- ----------------------------
INSERT INTO `tb_role` VALUES (1, 'admin', '2021-01-05 13:33:20', '2021-01-10 10:36:31');
INSERT INTO `tb_role` VALUES (2, 'user', '2021-01-05 13:33:25', '2021-01-10 10:36:53');

-- ----------------------------
-- Table structure for tb_role_auth
-- ----------------------------
DROP TABLE IF EXISTS `tb_role_auth`;
CREATE TABLE `tb_role_auth`  (
  `role_auth` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `auth_id` int(11) NOT NULL,
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`role_auth`) USING BTREE,
  INDEX `role_id`(`role_id`) USING BTREE,
  INDEX `auth_id`(`auth_id`) USING BTREE,
  CONSTRAINT `tb_role_auth_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `tb_role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tb_role_auth_ibfk_2` FOREIGN KEY (`auth_id`) REFERENCES `tb_authority` (`auth_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_role_auth
-- ----------------------------
INSERT INTO `tb_role_auth` VALUES (1, 1, 1, '2021-01-08 13:33:44', '2021-01-08 13:33:44');
INSERT INTO `tb_role_auth` VALUES (2, 1, 2, '2021-01-08 13:33:48', '2021-01-08 13:33:48');
INSERT INTO `tb_role_auth` VALUES (3, 1, 3, '2021-01-08 13:33:56', '2021-01-08 13:33:56');
INSERT INTO `tb_role_auth` VALUES (4, 1, 4, '2021-01-08 13:34:16', '2021-01-08 13:34:16');
INSERT INTO `tb_role_auth` VALUES (5, 1, 5, '2021-01-08 13:34:19', '2021-01-08 13:34:19');
INSERT INTO `tb_role_auth` VALUES (6, 1, 6, '2021-01-08 13:34:23', '2021-01-08 13:34:23');
INSERT INTO `tb_role_auth` VALUES (7, 1, 7, '2021-01-08 13:34:25', '2021-01-08 13:34:25');
INSERT INTO `tb_role_auth` VALUES (8, 2, 3, '2021-01-08 13:34:29', '2021-01-08 13:35:04');
INSERT INTO `tb_role_auth` VALUES (9, 2, 4, '2021-01-08 13:34:31', '2021-01-08 13:35:07');
INSERT INTO `tb_role_auth` VALUES (10, 2, 5, '2021-01-08 13:34:39', '2021-01-08 13:35:10');
INSERT INTO `tb_role_auth` VALUES (11, 2, 6, '2021-01-08 13:34:57', '2021-01-08 13:35:14');
INSERT INTO `tb_role_auth` VALUES (12, 2, 7, '2021-01-08 13:35:01', '2021-01-08 13:35:01');

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user`  (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `user_name` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `user_phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用来找回密码',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `deleted` int(2) NOT NULL DEFAULT 0,
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `user_id`(`user_id`) USING BTREE,
  UNIQUE INDEX `account`(`account`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES (1, '1001', '40385f1d87b521e2de1451b08d84f9be', '刘畅', '15864349952', '2021-01-05 13:33:40', '2021-01-28 12:32:15', 0);
INSERT INTO `tb_user` VALUES (2, '1101', 'e5f0a52a2aee27e0d0bd187397f0f023', '张三', '13569859874', '2021-01-05 13:34:03', '2021-01-28 11:30:35', 0);
INSERT INTO `tb_user` VALUES (3, '1102', 'e5f0a52a2aee27e0d0bd187397f0f023', '李四', '15895847885', '2021-01-05 13:34:18', '2021-01-28 11:30:36', 0);
INSERT INTO `tb_user` VALUES (4, '1004', 'c734af50d28fa5c13b45ef24588b66b6', '张毛毛', '13698563254', '2021-01-28 11:46:28', '2021-01-28 12:32:02', 0);
INSERT INTO `tb_user` VALUES (5, '1005', '5dc97189139bc2573a9d8613cc27978c', '张琳', '13698563254', '2021-03-06 16:21:25', '2021-03-06 16:21:25', 0);
INSERT INTO `tb_user` VALUES (6, '1006', '1111', '电放费', '13698563254', '2021-03-06 16:25:14', '2021-03-06 16:25:14', 0);

-- ----------------------------
-- Table structure for tb_user_role
-- ----------------------------
DROP TABLE IF EXISTS `tb_user_role`;
CREATE TABLE `tb_user_role`  (
  `user_role` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `deleted` int(2) NOT NULL DEFAULT 0,
  PRIMARY KEY (`user_role`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_user_role
-- ----------------------------
INSERT INTO `tb_user_role` VALUES (1, 1, 1, '2021-01-27 18:10:19', '2021-01-27 18:10:19', 0);
INSERT INTO `tb_user_role` VALUES (2, 2, 2, '2021-01-27 18:10:26', '2021-01-27 18:10:26', 0);
INSERT INTO `tb_user_role` VALUES (3, 3, 2, '2021-01-27 18:10:36', '2021-01-27 18:10:36', 0);
INSERT INTO `tb_user_role` VALUES (4, 4, 2, '2021-01-28 11:46:28', '2021-01-28 11:46:28', 0);
INSERT INTO `tb_user_role` VALUES (5, 5, 1, '2021-03-06 16:21:25', '2021-03-06 16:21:25', 0);
INSERT INTO `tb_user_role` VALUES (6, 6, 2, '2021-03-06 16:25:14', '2021-03-06 16:25:14', 0);

-- ----------------------------
-- Table structure for tb_usersalt
-- ----------------------------
DROP TABLE IF EXISTS `tb_usersalt`;
CREATE TABLE `tb_usersalt`  (
  `salt_id` int(11) NOT NULL AUTO_INCREMENT,
  `salt_text` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `salt_status` int(2) NOT NULL DEFAULT 0 COMMENT '0未使用，1密码已经被使用',
  `salt_role` int(2) NOT NULL DEFAULT 2 COMMENT '1代表生成的管理员的salt，2生成的为普通用户，默认为2',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `deleted` int(2) NOT NULL DEFAULT 0,
  PRIMARY KEY (`salt_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_usersalt
-- ----------------------------
INSERT INTO `tb_usersalt` VALUES (1, 'PY33SYYjbkhM1dVwrwpC', 1, 2, '2021-03-06 11:29:29', '2021-03-06 11:29:29', 0);
INSERT INTO `tb_usersalt` VALUES (2, '7qAYh6CAUi0GXiZUBcTA', 0, 1, '2021-03-06 20:47:06', '2021-03-06 20:47:06', 0);
INSERT INTO `tb_usersalt` VALUES (3, 'Z1ezRNbkiDfMceWw2tqO', 0, 2, '2021-03-06 20:50:27', '2021-03-06 20:50:27', 0);
INSERT INTO `tb_usersalt` VALUES (4, 'nI5zamz4EzGuHHNWL1iK', 0, 2, '2021-03-06 20:50:27', '2021-03-06 20:50:27', 0);
INSERT INTO `tb_usersalt` VALUES (5, 'BhOUpexQZWS0XOnm2cXV', 0, 2, '2021-03-06 20:50:27', '2021-03-06 20:50:27', 0);
INSERT INTO `tb_usersalt` VALUES (6, '0I5Wwt74oSlRh1Ihpvql', 0, 2, '2021-03-06 20:50:27', '2021-03-06 20:50:27', 0);
INSERT INTO `tb_usersalt` VALUES (7, '0rMadTNO3GBhfbM7Txxf', 0, 2, '2021-03-07 20:40:11', '2021-03-07 20:40:11', 0);
INSERT INTO `tb_usersalt` VALUES (8, 'IlD7y4sFwetcb3fLxvoj', 0, 2, '2021-03-07 20:40:16', '2021-03-07 20:40:16', 0);
INSERT INTO `tb_usersalt` VALUES (9, 'Y0ELW21LvSKpemwPajEl', 0, 2, '2021-03-06 21:02:58', '2021-03-06 21:02:58', 0);
INSERT INTO `tb_usersalt` VALUES (10, 'd6sejHqZiE2q2EXC6ybW', 0, 1, '2021-03-07 20:40:19', '2021-03-07 20:40:19', 0);

-- ----------------------------
-- Table structure for tb_vercode
-- ----------------------------
DROP TABLE IF EXISTS `tb_vercode`;
CREATE TABLE `tb_vercode`  (
  `ver_id` int(10) NOT NULL AUTO_INCREMENT,
  `account` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ver_code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `deleted` int(2) NOT NULL DEFAULT 0,
  PRIMARY KEY (`ver_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_vercode
-- ----------------------------
INSERT INTO `tb_vercode` VALUES (1, '1001', '3279', '2021-03-08 19:57:31', '2021-03-08 19:57:31', 1);
INSERT INTO `tb_vercode` VALUES (2, '1001', '1969', '2021-03-08 19:57:34', '2021-03-08 19:57:34', 1);
INSERT INTO `tb_vercode` VALUES (3, '1001', '7008', '2021-03-08 19:57:36', '2021-03-08 19:57:36', 1);
INSERT INTO `tb_vercode` VALUES (4, '1001', '5793', '2021-03-08 20:06:55', '2021-03-08 20:06:55', 1);
INSERT INTO `tb_vercode` VALUES (5, '1001', '8945', '2021-03-08 20:31:24', '2021-03-08 20:31:24', 1);
INSERT INTO `tb_vercode` VALUES (6, '1001', '2669', '2021-03-08 20:45:38', '2021-03-08 20:45:38', 1);
INSERT INTO `tb_vercode` VALUES (7, '1001', '9606', '2021-03-08 20:45:42', '2021-03-08 20:45:42', 1);
INSERT INTO `tb_vercode` VALUES (8, '1001', '2272', '2021-03-08 21:07:08', '2021-03-08 21:07:08', 1);
INSERT INTO `tb_vercode` VALUES (9, '1001', '2528', '2021-03-08 21:07:23', '2021-03-08 21:10:30', 1);
INSERT INTO `tb_vercode` VALUES (10, '1001', '0739', '2021-03-08 21:23:28', '2021-03-08 21:23:36', 1);

-- ----------------------------
-- Table structure for tb_visitor
-- ----------------------------
DROP TABLE IF EXISTS `tb_visitor`;
CREATE TABLE `tb_visitor`  (
  `vis_id` int(11) NOT NULL AUTO_INCREMENT,
  `add_id` int(11) NOT NULL,
  `vis_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `vis_phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `vis_remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `deleted` int(2) NULL DEFAULT 0,
  PRIMARY KEY (`vis_id`) USING BTREE,
  INDEX `res_id`(`add_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '来访客人登记表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_visitor
-- ----------------------------
INSERT INTO `tb_visitor` VALUES (1, 1, '李琳', '14785458214', '探望朋友', '2021-01-11 16:17:01', '2021-01-24 14:07:00', 0);
INSERT INTO `tb_visitor` VALUES (3, 5, '张三', '16875984574', '送外卖', '2021-01-21 21:05:51', '2021-01-24 12:33:58', 0);
INSERT INTO `tb_visitor` VALUES (5, 3, '张伟伟', '13569874562', '探亲', '2021-01-23 17:45:21', '2021-01-23 17:45:21', 0);
INSERT INTO `tb_visitor` VALUES (6, 1, '张艳艳', '13698569632', '探亲', '2021-01-24 11:58:58', '2021-01-24 11:58:58', 0);
INSERT INTO `tb_visitor` VALUES (7, 1, '张丽', '13698569875', '有事', '2021-01-24 13:49:23', '2021-01-24 14:07:03', 0);

SET FOREIGN_KEY_CHECKS = 1;
